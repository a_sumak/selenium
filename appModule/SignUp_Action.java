package appModule;

import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import pageObjects.Home_Page;
import pageObjects.Login_Page;
import pageObjects.SignUp_Page;
import utility.Constant;

 public class SignUp_Action{
 
     public static void Execute(WebDriver driver, String FirstName, String LastName, String Email, String Phone, String Password){
    	 
    	 
    	 	try {
    	 		
    	 		Home_Page.btn_SignIn(driver).click();
    	 		Assert.assertEquals(driver.getCurrentUrl(), Constant.SignInPageURL);  	 	
        	 	Login_Page.lnk_SignUp(driver).click();
        	 	Assert.assertEquals(driver.getCurrentUrl(), Constant.SignUpPageURL);
        	 	SignUp_Page.inpt_FirstName(driver).sendKeys(FirstName);
        	 	SignUp_Page.inpt_LastName(driver).sendKeys(LastName);
        	 	SignUp_Page.inpt_Email(driver).sendKeys(Email);
        	 	SignUp_Page.inpt_Phone(driver).sendKeys(Phone);
        	 	SignUp_Page.inpt_Password(driver).sendKeys(Password);
        	 	SignUp_Page.box_Agree(driver).click();
        	 	SignUp_Page.btn_CreateAccount(driver).click();
        	 	
    	 	} catch(NoSuchElementException exception) {
    	 		System.out.println("No Element found");
    	 	}
    	 	
    	 	
    	 	
	
    	   	
    	 	
   }
 
}