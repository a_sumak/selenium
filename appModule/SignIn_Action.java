package appModule;

import static org.testng.Assert.assertEquals;

import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import pageObjects.Home_Page;
import pageObjects.Login_Page;
import pageObjects.My_Vylla;
import utility.Constant;

 public class SignIn_Action{
 
     public static void Execute(WebDriver driver, String Username, String Password){
    	 	
    	 	
    	 	try {
    	 		
    	 		Home_Page.btn_SignIn(driver).click();
    	 		Assert.assertEquals(driver.getCurrentUrl(), Constant.SignInPageURL);
    	 		Login_Page.inpt_Username(driver).sendKeys(Username);	 		
    	 		Login_Page.inpt_Password(driver).sendKeys(Password);
    	 		Login_Page.btn_SubmitSignIn(driver).click();
    	 		
    	 	} catch(NoSuchElementException exception) {
    	 		
    	 		System.out.println("No Element found");
    	 	}
    	   	
    	 	
   }
 
}