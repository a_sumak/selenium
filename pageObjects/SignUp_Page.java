package pageObjects;
 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class SignUp_Page {
	
	
	private static WebElement element = null;
 
	public static WebElement inpt_FirstName(WebDriver driver){
		 
		element = driver.findElement(By.id("firstName"));
		return element;
 
    }
	
	public static WebElement inpt_LastName(WebDriver driver){
		 
		element = driver.findElement(By.id("lastName"));
		return element;
 
    }
	
	public static WebElement inpt_Email(WebDriver driver){
		 
		element = driver.findElement(By.id("email"));
		return element;
 
    }
	
	public static WebElement inpt_Phone(WebDriver driver){
		 
		element = driver.findElement(By.id("primPhoneNumber"));
		return element;
 
    }
	
	public static WebElement inpt_Password(WebDriver driver){
		 
		element = driver.findElement(By.id("newAcctPassword"));
		return element;
 
    }
	
	public static WebElement box_Agree(WebDriver driver){
		 
		element = driver.findElement(By.id("consent-checkbox"));
		return element;
 
    }
	
	public static WebElement btn_CreateAccount(WebDriver driver){
		 
		element = driver.findElement(By.id("btnSave"));
		return element;
 
    }
	
	public static WebElement txt_WrongPassFormat(WebDriver driver){
		 
		element = driver.findElement(By.xpath("//span[.='The Password must follow the Password guidelines']"));
		return element;
 
    }
	
	public static WebElement alr_PassGuide(WebDriver driver){
		 
		element = driver.findElement(By.id("passwordGuideline"));
		return element;
 
    }
	
	public static WebElement txt_FieldReq(WebDriver driver){
		 
		element = driver.findElement(By.xpath("//span[.='This field is required']"));
		return element;
 
    }
	
	
	
	
	
	
}
