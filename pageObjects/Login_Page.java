package pageObjects;
 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login_Page {
	
	private static WebElement element = null;
	 
	public static WebElement lnk_SignIn(WebDriver driver){
		
		element = driver.findElement(By.id("login-link"));
		return element;	
		
	}
	
	public static WebElement lnk_SignUp(WebDriver driver){
		
		element = driver.findElement(By.id("sign-up-link"));
		return element;	
		
	}
	
	public static WebElement btn_SignIn(WebDriver driver){
		 
		element = driver.findElement(By.xpath("/html/body/div[3]/div[2]/header/div/div[1]/a/span[1]/span"));
		return element;
 
    }
	
	public static WebElement lbl_Email(WebDriver driver){
		 
		element = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div/div/div[2]/label"));
		return element;
 
    }
	
	public static WebElement lbl_Password(WebDriver driver){
		 
		element = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div/div/div[3]/label"));
		return element;
 
    }
	
	public static WebElement inpt_Username(WebDriver driver){
		 
		element = driver.findElement(By.id("UserName"));
		return element;
 
    }
	
	public static WebElement inpt_Password(WebDriver driver){
		 
		element = driver.findElement(By.id("Password"));
		return element;
 
    }
	
	public static WebElement btn_SubmitSignIn(WebDriver driver){
		 
		element = driver.findElement(By.id("btnLogOn"));
		return element;
 
    }
	
	public static WebElement btn_ForgotPass(WebDriver driver){
		 
		element = driver.findElement(By.xpath("html/body/div[4]/div/div[2]/div/div/div/div[5]/a"));
		return element;
 
    }
	
	public static WebElement txt_WrongNameOrPass(WebDriver driver){
		 
		element = driver.findElement(By.xpath("//span[.='The user name or password provided is incorrect.']"));
		return element;
 
    }
	
	public static WebElement txt_BothNameAndPassReq(WebDriver driver){
		
		element = driver.findElement(By.xpath("//span[.='Both Email Address and Password are required.']"));
		return element;
 
    }
	
	public static WebElement txt_FieldReq(WebDriver driver){
		 
		element = driver.findElement(By.xpath("//span[.='This field is required']"));
		return element;
 
    }
	

	

	

	

	

}