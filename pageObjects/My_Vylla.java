package pageObjects;
 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class My_Vylla {
	private static WebElement element = null;
 
	public static WebElement lnk_MyPage(WebDriver driver){
 
		element = driver.findElement(By.xpath("/html/body/div[3]/div[2]/header/div/div[1]/a/span[1]/span"));
		return element;
		
    }
}
