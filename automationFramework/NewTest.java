package automationFramework;

import org.testng.annotations.Test;
import appModule.SignIn_Action;
import appModule.SignUp_Action;
import pageObjects.Home_Page;
import pageObjects.Login_Page;
import pageObjects.My_Vylla;
import pageObjects.SignUp_Page;
import utility.Constant;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class NewTest {
	
	public WebDriver driver = null;
	public Select dropdown = null;
 
  @BeforeMethod
  public void beforeMethod() {
	  System.setProperty("webdriver.chrome.driver", "/Users/alex.sumak/Downloads/chromedriver");
	  driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.get(Constant.MainPageURL);
  }

  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }

 
  
  // Sign In with Valid Credentials C20
  
  @Test
  public void Sign_In_Test_1() throws Exception {
	  
	  SignIn_Action.Execute(driver, Constant.Email, Constant.Password);
	  Thread.sleep(3000);
	  Assert.assertEquals(driver.getCurrentUrl(), Constant.MyVyllaPageURL);
	  My_Vylla.lnk_MyPage(driver).isDisplayed();
	  
  }
  
  
  
  
  // Sign In with Valid Email and Invalid Password C22
  
  @Test
  public void Sign_In_Test_2() throws Exception {
	  SignIn_Action.Execute(driver, Constant.Email, Constant.Wrong_Password);
	  Assert.assertEquals(driver.getCurrentUrl(), Constant.SignInPageURL);
	  Login_Page.txt_WrongNameOrPass(driver).isDisplayed();
  }
  
  
  
   
  // Sign In With a Blank Password C23
  
  @Test
  public void Sign_In_Test_3() throws Exception {
	  SignIn_Action.Execute(driver, Constant.Email, "");
	  Assert.assertEquals(driver.getCurrentUrl(), Constant.SignInPageURL);
	  Login_Page.txt_BothNameAndPassReq(driver).isDisplayed();  
  }
  
  
  //Sign In With a Blank Email C24
  
 @Test
 public void Sign_In_Test_4() throws Exception {
	  SignIn_Action.Execute(driver, "", Constant.Password);
	  Assert.assertEquals(driver.getCurrentUrl(), Constant.SignInPageURL);
	  Login_Page.txt_FieldReq(driver).isDisplayed();
	  
 }

  
  // Sign In with Invalid(Not registered) Email C21
  @Test
  public void Fifth_Test_Case() throws Exception {
 	  SignIn_Action.Execute(driver, Constant.UnregEmail, Constant.Password);
 	  Assert.assertEquals(driver.getCurrentUrl(), Constant.SignInPageURL);
 	  Login_Page.txt_WrongNameOrPass(driver).isDisplayed();
 	  
  }
  
  /*
  
  // Sign Up With Valid Credentials C17
  @Test
  public void Sixth_Test_Case() throws Exception {
	  SignUp_Action.Execute(driver, Constant.FirstName, Constant.LastName, Constant.NewEmail, 
			  Constant.Phone, Constant.Password);
	  
	  Thread.sleep(10000);
	  
	  Assert.assertEquals(driver.getCurrentUrl(), Constant.MyVyllaPageURL);
	  
	  String name = My_Vylla.lnk_MyPage(driver).getText();
	  String[] names = name.split(" ");
	  String firstName = names[0];
	  String lastName = names[1];
	  Assert.assertEquals(firstName, Constant.FirstName.toUpperCase());
	  Assert.assertEquals(lastName, Constant.LastName.toUpperCase());
 	  
  }
  
  */
 
  
  // Sign Up With a Blank Password C18
  @Test
  public void Seventh_Test_Case() throws Exception {
	  SignUp_Action.Execute(driver, Constant.FirstName, Constant.LastName, Constant.NewEmail, 
			  Constant.Phone, "");
	  
	  Assert.assertEquals(driver.getCurrentUrl(), Constant.SignUpPageURL);
	  SignUp_Page.txt_WrongPassFormat(driver).isDisplayed();
	  SignUp_Page.alr_PassGuide(driver).isDisplayed();
	  
  }
 
  
	// 	Sign Up With a Blank First Name or Last Name c19
	 @Test
	 public void Eighth_Test_Case1() throws Exception {
		  SignUp_Action.Execute(driver, "", Constant.LastName, Constant.NewEmail, 
				  Constant.Phone, Constant.Password);
		  
		  Assert.assertEquals(driver.getCurrentUrl(), Constant.SignUpPageURL);
		  SignUp_Page.txt_FieldReq(driver).isDisplayed();
		  
		  
	 }
	 
	 @Test
	 public void Eighth_Test_Case2() throws Exception {
		 SignUp_Action.Execute(driver, Constant.FirstName, "", Constant.NewEmail, 
				  Constant.Phone, Constant.Password);
		  
		  Assert.assertEquals(driver.getCurrentUrl(), Constant.SignUpPageURL);
		  SignUp_Page.txt_FieldReq(driver).isDisplayed();
		 
	 }
 

 

}
